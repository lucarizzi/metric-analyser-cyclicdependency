import pandas as pd
from ipdb import set_trace
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

matplotlib.style.use('ggplot')

def computeStatistics(proj_name, sp_df, dir_scatterPlot, dir_linePlot, metrix_1, metrix_2):
    insuff_num_CD = False
    # set_trace()
    if len(sp_df.index) <= 1:
        insuff_num_CD = True
        return insuff_num_CD
    
    as_list = sp_df['AS'].tolist()
    sp_df.drop(['AS'], axis=1, inplace=True)
    # SCATTER_PLOT
    scatterPlot(sp_df, dir_scatterPlot, proj_name, metrix_1, metrix_2)

    # PLOT DISTRIBUTION SP METRICS
    # line_plot(sp_df, dir_linePlot, proj_name, as_list)

    # CALCOLO MATRICE DI CORRELAZIONE: SPEARMAN E KENDALL
    corrMatrix_spearman = sp_df.corr(method='spearman')
    corrMatrix_kendall = sp_df.corr(method='kendall')

    # CREO UNA LISTA CON L'INTERO INSIEME DI MATRICI DI CORRELAZIONE
    proj_tuple_spearman = (proj_name, corrMatrix_spearman)
    # list_corrMatrix_spearman.append(proj_tuple_spearman)

    proj_tuple_kendall = (proj_name, corrMatrix_kendall)
    # list_corrMatrix_kendall.append(proj_tuple_kendall)
    # CREO UNA LISTA CON CORRELAZIONE TRA TP, TS E PS
    ps_tuple_spearman = (
        proj_name, corrMatrix_spearman[metrix_1][metrix_2])

    ps_tuple_kendall = (
        proj_name, corrMatrix_kendall[metrix_1][metrix_2])

    return insuff_num_CD, ps_tuple_spearman, ps_tuple_kendall
    

def scatterPlot(sp_metrics, dir_scatterPlot, proj_name, metrix_1, metrix_2):
    scatter_plot = pd.plotting.scatter_matrix(sp_metrics, figsize=(6, 6))
    plt.savefig(dir_scatterPlot + '/' + proj_name + '_scatterPlot_' + metrix_1 + '_' + metrix_2 + '.svg')
    plt.close()

def line_plot(sp_metrics, dir_linePlot, proj_name, as_list):
    fig, ax = plt.subplots()
    ax.plot(as_list, sp_metrics['SevScaled'], 'ro', label='Severity')
    ax.plot(as_list, sp_metrics['PageScaled'], 'bo', label='PageRank')
    legend = ax.legend(loc='upper center', fontsize='small')
    plt.savefig(dir_linePlot + '/' + proj_name + '_linePlot.svg')
    plt.close()
    
    

def print_statistics(ps_spearman_df, ps_kendall_df, dir_statistics, id):
    statistics_spearman = ps_spearman_df.describe()
    statistics_kendall = ps_kendall_df.describe()

    statistics_spearman.to_csv(dir_statistics + '/spearman_statistics_' + id + '.csv')
    statistics_kendall.to_csv(dir_statistics + '/kendall_statistics_' + id + '.csv')
    return statistics_spearman, statistics_kendall

def print_boxplot(ps_spearman_df, ps_kendall_df, dir_boxPlot, id):
    fig, ax = plt.subplots()
    ps_spearman_df.boxplot(column=['PS'])
    plt.savefig(dir_boxPlot + '/boxPlot_spearman_' + id + '.svg')

    fig, ax = plt.subplots()
    ps_kendall_df.boxplot(column=['PS'])
    plt.savefig(dir_boxPlot + '/boxPlot_kendall_' + id + '.svg')


def get_outlier(ps_spearman_df, ps_kendall_df, statistics_spearman, statistics_kendall):

    iqr_ps_spearman = statistics_spearman.iloc[6]['PS'] - statistics_spearman.iloc[4]['PS']
    whis_iqr_ps_spearman = iqr_ps_spearman * 1.5
    outlier_spearman_ps_below = ps_spearman_df.loc[ps_spearman_df['PS']
                                                         < statistics_spearman.iloc[4]['PS'] - whis_iqr_ps_spearman]
    outlier_spearman_ps_below['metrics'] = 'PS'
    outlier_spearman_ps_below['outliers'] = 'below'

    outlier_spearman_ps_above = ps_spearman_df.loc[ps_spearman_df['PS']
                                                         > statistics_spearman.iloc[6]['PS'] + whis_iqr_ps_spearman]
    outlier_spearman_ps_above['metrics'] = 'PS'
    outlier_spearman_ps_above['outliers'] = 'above'

    outlier_spearman = outlier_spearman_ps_below.copy()
    outlier_spearman = outlier_spearman.append(
        outlier_spearman_ps_above, ignore_index=False)

    iqr_ps_kendall = statistics_kendall.iloc[6]['PS'] - statistics_kendall.iloc[4]['PS']
    whis_iqr_ps_kendall = iqr_ps_kendall * 1.5
    outlier_kendall_ps_below = ps_kendall_df.loc[ps_kendall_df['PS']
                                                       < statistics_kendall.iloc[4]['PS'] - whis_iqr_ps_kendall]
    outlier_kendall_ps_below['metrics'] = 'PS'
    outlier_kendall_ps_below['outliers'] = 'below'

    outlier_kendall_ps_above = ps_kendall_df.loc[ps_kendall_df['PS']
                                                       > statistics_kendall.iloc[6]['PS'] + whis_iqr_ps_kendall]
    outlier_kendall_ps_above['metrics'] = 'PS'
    outlier_kendall_ps_above['outliers'] = 'above'

    outlier_kendall = outlier_kendall_ps_below.copy()
    outlier_kendall = outlier_kendall.append(
        outlier_kendall_ps_above, ignore_index=False)

    return outlier_spearman, outlier_kendall


def makeAnalysis(count_shapes, dir_statistics, ps_spearman, ps_kendall, dir_correlation_statistics, id, dir_outliers, dir_boxPlot):

    count_shapes_df = pd.DataFrame(
        count_shapes, columns=['Project', 'CountTiny', 'CountShape', 'TotShape'])
    count_shapes_df.to_csv(dir_statistics + '/shapeCount.csv')
    # FROM LIST OF TUPLE TO DATAFRAME
    ps_spearman_df = pd.DataFrame([x[1] for x in ps_spearman], columns=[
                                  'PS'], index=[y[0] for y in ps_spearman])

    ps_kendall_df = pd.DataFrame([x[1] for x in ps_kendall], columns=[
                                 'PS'], index=[y[0] for y in ps_kendall])

    mean_kendall = ps_kendall_df['PS'].mean(axis=0)
    mean_spearman = ps_spearman_df['PS'].mean(axis=0)

    # EXPORT TP_TS_PS_DATAFRAME
    ps_spearman_df.to_csv(dir_correlation_statistics + '/ps_spearman_' + id + '.csv')
    ps_kendall_df.to_csv(dir_correlation_statistics + '/ps_kendall_' + id + '.csv')

    # EXTRACT INFORMATION
    statistics_spearman, statistics_kendall = print_statistics(
        ps_spearman_df, ps_kendall_df, dir_statistics, id)

    # CHECK OUTLIERS
    outlier_spearman, outlier_kendall = get_outlier(
        ps_spearman_df, ps_kendall_df, statistics_spearman, statistics_kendall)

    outlier_spearman.to_csv(dir_outliers + '/outliers_spearman_' + id + '.csv')
    outlier_kendall.to_csv(dir_outliers + '/outliers_kendall_' + id + '.csv')

    # PRINT BOXPLOT
    print_boxplot(ps_spearman_df, ps_kendall_df, dir_boxPlot, id)

    return mean_kendall, mean_spearman
