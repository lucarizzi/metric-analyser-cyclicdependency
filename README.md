## Project Name

Metric Analyser for CyclicDependency

## Synopsis

A tool to compute different analysis on different metrics regarding Cyclic Dependency smell

## Requirements

- Arcan 1.5.0
- Python 3.6.5