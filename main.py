import sys, os, shutil, csv, spComputation, metricStatistics, diameterComputation, graphComputation, statistics, json
import seaborn as sns
import pandas as pd
from ipdb import set_trace
import numpy as np
import matplotlib.pyplot as plt

projects = ['ant-1.8.2', 'antlr-3.4']
# projects = ['ant-1.8.2', 'antlr-3.4', 'apache-maven-3.0', 'axion-1.0-M2', 'batik-1.7', 'cobertura-1.9.4.1', 'colt-1.2.0', 'derby-10.9.1.0', 'displaytag-1.2', 'drawswf-1.2.9', 'drjava-stable-20100913-r5387', 'eclipse_SDK-3.7.1', 'emma-2.0.5312', 'findbugs-1.3.9', 'freecs-1.3.20100406', 'heritrix-1.14.4', 'hibernate-4.2.0', 'hsqldb-2.0.0', 'informa-0.7.0-alpha2', 'itext-5.0.3', 'james-2.2.0', 'jasperreports-3.7.4', 'jena-2.6.3', 'jext-5.0', 'jfreechart-1.0.13', 'jgraph-5.13.0.0', 'jgraphpad-5.10.0.2', 'jmeter-2.5.1', 'jmoney-0.4.4', 'jparse-0.96', 'jspwiki-2.8.4', 'jsXe-04_beta', 'jtopen-7.8', 'junit-4.10', 'log4j-2.0-beta', 'marauroa-3.8.1', 'megamek-0.35.18', 'nakedobjects-4.0.0', 'nekohtml-1.9.14', 'netbeans-7.3', 'openjms-0.7.7-beta-1', 'oscache-2.3', 'picocontainer-2.10.2', 'pmd-4.2.5', 'pooka-3.0-080505', 'proguard-4.9', 'quartz-1.8.3', 'quickserver-1.4.7', 'squirrel_sql-3.1.2', 'sunflow-0.07.2', 'tapestry-5.1.0.5', 'webmail-0.7.10', 'xalan-2.7.1', 'xerces-2.10.0', 'xmojo-5.0.0']

dir_output = '/results/output'

doc_packCDTable = '/packageCyclicDependencyTable.csv'
doc_topicDistance = '/correlationAnalysis/topic_distance.csv'
doc_adiResults = '/testADIResults.csv'
doc_sp = '/correlationAnalysis/sp.csv'
dir_results = 'C:/Users/lucar/Desktop/Borsa/SPD_correlation/results/results_19/'

list_project_noCD = []
low_num_CD = []

corr_dict_spearman = {'Diameter_PageRank': [], 'Diameter_Occurrence': [], 'Diameter_Severity': [
], 'PageRank_Occurrence': [], 'PageRank_Severity': [], 'Occurrence_Severity': []}

corr_dict_kendall = {'Diameter_PageRank': [], 'Diameter_Occurrence': [], 'Diameter_Severity': [
], 'PageRank_Occurrence': [], 'PageRank_Severity': [], 'Occurrence_Severity': []}
dict_k = list(corr_dict_kendall.keys())

list_metrics = ['Diameter', 'PageRank', 'Occurrence', 'Severity']

severity_distribution_dict = {'Diameter': [], 'PageRank': [], 'Occurrence': [], 'Severity': []}

count_shapes = []

dir_statistics = dir_results + '00_allProjectsResults'
dir_boxPlot = dir_statistics + '/boxPlot'
dir_dictionaries = dir_statistics + '/dictionaries'
dir_scatterPlot = dir_statistics + '/scatterPlot'

dir_linePlot = dir_statistics + '/linePlot'
dir_correlation_statistics = dir_statistics + '/correlation_statistics'
dir_outliers = dir_statistics + '/outliers'
dir_heatmap = dir_statistics + '/heatmap'
if not os.path.exists(dir_statistics):
        os.makedirs(dir_boxPlot)
        os.makedirs(dir_dictionaries)
        os.makedirs(dir_scatterPlot)
        os.makedirs(dir_heatmap)
        os.makedirs(dir_linePlot)
        os.makedirs(dir_correlation_statistics)
        os.makedirs(dir_outliers)

for i in projects:
    dir_proj_output = dir_results + i + dir_output
    dir_correlationAnalysis = dir_proj_output + '/correlationAnalysis'

    # CREATION OF THE OUTPUT PATH
    if not os.path.exists(dir_correlationAnalysis):
        os.makedirs(dir_correlationAnalysis)

    dir_adi = dir_proj_output + doc_adiResults
    dir_packCDTable = dir_proj_output + doc_packCDTable

    dir_sp = dir_proj_output + doc_sp
    adi_df = pd.read_csv(dir_adi)
    packCDTable_df = pd.read_csv(dir_packCDTable)

    # EXTRACTION OF ADI RESULTS ONLY OF PACKAGES
    adi_smellPackCD = adi_df.loc[adi_df['AS'].isin(
        packCDTable_df['AS'].tolist())]
    
    # GET DIAMETER METRIC - RANK_MAX HAS THE MAX VALUE OF DIAMETER METRIC INTO CIRCLE SHAPE / RANK_MEAN HAS THE MEAN VALUE OF DIAMETER METRIC INTO CIRCLE SHAPE
    dir_graphml = dir_proj_output + '/ToySystem-graph.graphml'
    dir_ct = dir_proj_output + '/CTree.graphml'
    diameter_rank_max, total_dep_pname = diameterComputation.getDiameter(
        dir_graphml, dir_ct)
    total_dep_pname_df = pd.DataFrame(total_dep_pname, columns=['AS', 'pack_1', 'pack_2', 'weight'])
    diameter_max_df = pd.DataFrame(diameter_rank_max, columns=['AS', 'Diameter'])
    
    # GET SEVERITY AND PAGERANK NOT SCALED INTO 0-1
    sp_df = spComputation.getSP(adi_smellPackCD)
    sp_df['AS'] = sp_df['AS'].astype(str)

    # GET CLASSES IMPLIED INTO PACKAGE DEPENDENCIES
    num_classes, class_dependencies = graphComputation.getNumClasses(dir_graphml, total_dep_pname_df)
    class_dependencies.to_csv(dir_correlationAnalysis + '/classDependencies.csv')

    # MERGE METRICES IN AN UNIQUE DATAFRAME
    spd_df = pd.merge(diameter_max_df, sp_df, on='AS')
    spd_df = pd.merge(spd_df, num_classes, on='AS')
    spd_df['Severity_elClass'] = spd_df['Severity_el'] * spd_df['num_classes']
    spd_df['Severity_2_original'] = 1 - ((1/(spd_df['num_classes'] + spd_df['Severity_el'])) * (spd_df['Severity_el'] / spd_df['num_classes']))
    spd_df['Severity'] = (((spd_df['Severity_2_original'] - 0.75)*1)/0.25) # + 0
    spd_df.to_csv(dir_correlationAnalysis + '/metricsResults.csv')
    
    # COUNT SHAPES IN PROJECT
    shape_tuple = (i, len(adi_smellPackCD[adi_smellPackCD['NumberOfVerteces'] == 2]), len(
        adi_smellPackCD[adi_smellPackCD['NumberOfVerteces'] > 2]), len(adi_smellPackCD['NumberOfVerteces']))
    if (shape_tuple[1] + shape_tuple[2]) != shape_tuple[3]:
        print('COUNT SHAPE ERROR')
        break
    count_shapes.append(shape_tuple)
    

    # CALCOLO DELLA DISTRIBUZIONE DELLE VARIANZE
    for sev in severity_distribution_dict:
        severity_distribution_dict[sev].append(statistics.variance(spd_df[sev].tolist()))
    
    # SPD ANALYSIS
    for index, m_1 in enumerate(spd_df.columns):
        if m_1 == 'AS':
            continue
        for m_2 in spd_df.columns[index+1:]:
            id = m_1 + '_' + m_2
            if id not in dict_k:
                continue
            spd_df_temp = spd_df[[spd_df.columns[0], m_1, m_2]].copy()
            num_cd_flag, corr_tuple_spearman, corr_tuple_kendall = metricStatistics.computeStatistics(i, spd_df_temp, dir_scatterPlot, dir_linePlot, m_1, m_2)
            corr_dict_spearman[id].append(corr_tuple_spearman)
            corr_dict_kendall[id].append(corr_tuple_kendall)
            if num_cd_flag:
                low_num_CD.append(i)
    print('FINISH PROJECT: ' + i)
    
# PRINT DELLE DISTRIBUZIONI DELLE 4 SEVERITY
with open(dir_dictionaries + '/sev_test.json', 'w') as severityOut:
    json.dump(severity_distribution_dict, severityOut)

for singleSeverity in severity_distribution_dict:
    print(singleSeverity + ': ')
    print(statistics.mean(severity_distribution_dict[singleSeverity]))
# PRINT MEDIA DELLE DISTRIBUZIONI DELLE 4 SEVERITY
sev_distribution_df = pd.DataFrame.from_dict(severity_distribution_dict)
sev_mean_distribution = sev_distribution_df.mean(axis=0)
sev_mean_distribution.to_csv(dir_dictionaries + '/sev_mean_distribution.csv')

# PRINT DELLE CORRELAZIONI
with open(dir_dictionaries + '/corr_spearman.json', 'w') as csd:
    json.dump(corr_dict_spearman, csd)
with open(dir_dictionaries + '/corr_kendall.json', 'w') as ckd:
    json.dump(corr_dict_kendall, ckd)

# FROM DICT TO DATAFRAME
corr_kendall_df = pd.DataFrame.from_dict(corr_dict_kendall)
corr_spearman_df = pd.DataFrame.from_dict(corr_dict_spearman)



# ANALISI SULLE CORRELAZIONI: BOXPLOT, STATISTICS, OUTLIERS
kendall_mean_list = []
spearman_mean_list = []
list_dict_id = []

for dict_id in corr_dict_spearman:
    mean_kendall, mean_spearman = metricStatistics.makeAnalysis(count_shapes, dir_statistics, corr_dict_spearman[dict_id], corr_dict_kendall[dict_id], dir_correlation_statistics, dict_id, dir_outliers, dir_boxPlot)
    kendall_mean_list.append((dict_id, mean_kendall))
    spearman_mean_list.append((dict_id, mean_spearman))
# GENERATE HITMAP
df_correlations_kendall = pd.DataFrame(index=list_metrics, columns=list_metrics)
df_correlations_spearman = pd.DataFrame(index=list_metrics, columns=list_metrics)

for mean_corr_k in kendall_mean_list:
    flag_k = False
    for index_k, metric_1_k in enumerate(list_metrics[:-1]):
        for metric_2_k in list_metrics[index_k+1:]:
            if mean_corr_k[0].startswith(metric_1_k) and mean_corr_k[0].endswith(metric_2_k):
                df_correlations_kendall.iloc[index_k][metric_2_k] = mean_corr_k[1]
                flag_k = True
            if flag_k:
                break
        if flag_k:
            break
for mean_corr_s in spearman_mean_list:
    flag_s = False
    for index_s, metric_1_s in enumerate(list_metrics[:-1]):
        for metric_2_s in list_metrics[index_s+1:]:
            if mean_corr_s[0].startswith(metric_1_s) and mean_corr_s[0].endswith(metric_2_s):
                df_correlations_spearman.iloc[index_s][metric_2_s] = mean_corr_s[1]
                flag_s = True
            if flag_s:
                break
        if flag_s:
            break
df_correlations_kendall.fillna(value=np.nan, inplace=True)
df_correlations_spearman.fillna(value=np.nan, inplace=True)

df_correlations_kendall_t = df_correlations_kendall.transpose()
df_correlations_spearman_t = df_correlations_spearman.transpose()

plt.figure()
kendall_hm = sns.heatmap(df_correlations_kendall_t, vmin=-1, vmax=1)
plt.savefig(dir_heatmap + '/kendall_heatmap.svg')

plt.figure()
spearman_hm = sns.heatmap(df_correlations_spearman_t, vmin=-1, vmax=1)
plt.savefig(dir_heatmap + '/spearman_heatmap.svg')
