from igraph import *
from ipdb import set_trace

def getCT(g, url, out_dir):
    # creo grafo
    # estraggo i package di sistema
    sysPackage = g.vs.select(PackageType_eq="SystemPackage")

    # estraggo i nomi dei package
    sysPackage_names = []
    for i in sysPackage:
        sysPackage_names.append(i["name"].split("."))
    sysPackage_names.sort(key=len)
    containmentTree = generateCT(sysPackage, sysPackage_names, g)
    containmentTree.write_graphml(out_dir)

    return containmentTree


def generateCT(sysPack, sysPack_names, g):
    cTree = Graph(directed=True)
    cTree.add_vertices(1)
    rootNode = cTree.vs[len(cTree.vs)-1]
    rootNode['name'] = 'root'
    rootNode['depthCT'] = 0
    for pathPackName in sysPack_names:
#     CASO BASE _ SE PACKAGE HA LUNGHEZZA 1        
        if len(pathPackName) == 1:
            cTree.add_vertices(1)
            lastNode = cTree.vs[len(cTree.vs)-1]
            pNode = g.vs.select(name_eq=pathPackName[0])[0]
            lastNode.update_attributes(pNode.attributes())
            lastNode['depthCT'] = 1
            cTree.add_edge(cTree.vs[len(cTree.vs)-1],
                           cTree.vs.select(name_eq='root')[0], weight=1)
#     CASO PASSO _ SE PACKAGE HA LUNGHEZZA MAGGIORE DI 1
        else:
            nodeInDepth1 = cTree.vs.select(depthCT_eq=1)
#       CASO PASSO 1 _ NODO APPARTIENE GIA' A GRAFO
            if pathPackName[0] in nodeInDepth1['name']:
                nodeIndex = nodeInDepth1["name"].index(pathPackName[0])
                existingRoot = nodeInDepth1[nodeIndex]
                traverse_path(cTree, g, pathPackName, existingRoot)
            else:
                create_path(cTree, g, pathPackName, 0)
    return cTree


def traverse_path(cTree, g, path, existingRoot):
    for index, pack in enumerate(path[1:]):
        if index == len(path[1:])-1:
            cTree.add_vertices(1)
            leafNode = cTree.vs[len(cTree.vs)-1]
            pathString = '.'.join(path)
            gNode = g.vs.select(name_eq=pathString)[0]
            leafNode.update_attributes(gNode.attributes())
            leafNode['depthCT'] = index+2
            leafNode['name'] = pack
            secToLastNode = find_prec_from_path(cTree, path[:len(path)-1])

            cTree.add_edge(
                leafNode, cTree.vs[secToLastNode], weight=1/(2 ** (index+1)))
        else:
            predecessorsList = cTree.predecessors(existingRoot.index)
            existPrecNode = False
            for indexNodes in predecessorsList:
                if cTree.vs[indexNodes]['name'] == pack:
                    existPrecNode = True
                    existingRoot = cTree.vs[indexNodes]
                    break
            if len(predecessorsList) == 0 or not existPrecNode:
                create_path(cTree, g, path, index+1)
                break


def find_prec_from_path(cTree, path):
    rootNodes = cTree.vs.select(depthCT_eq=1)

    rNode = None
    index_node = None
    if len(path) == 1:
        for i in rootNodes:
            if i['name'] == path[0]:
                index_node = i.index
                break
    else:
        for i in rootNodes:
            if i['name'] == path[0]:
                rNode = i.index
                break
        pred_list = cTree.predecessors(rNode)
        for j in path[1:]:
            for n in pred_list:
                if cTree.vs[n]['name'] == j:
                    index_node = n
                    pred_list = cTree.predecessors(index_node)
                    break
    return index_node


def create_path(cTree, g, path, startIndex):
    depth = startIndex
    depth += 1
    for index, pack in enumerate(path[startIndex:]):
        if index == len(path[startIndex:])-1:
            cTree.add_vertices(1)
            leafNode = cTree.vs[len(cTree.vs)-1]
            pathString = '.'.join(path)
            gNode = g.vs.select(name_eq=pathString)[0]
            leafNode.update_attributes(gNode.attributes())
            leafNode['depthCT'] = depth
            leafNode['name'] = pack
            precNode = find_prec_from_path(cTree, path[:depth-1])
            cTree.add_edge(leafNode, precNode, weight=1/(2 ** (depth-1)))
        else:
            cTree.add_vertices(1)
            internalNode = cTree.vs[len(cTree.vs)-1]
            internalNode["name"] = pack
            internalNode["depthCT"] = depth
#             connetto primo nodo con root_node
            if depth == 1:
                precNode = cTree.vs.select(depthCT_eq=0)[0]
            else:
                precNode = find_prec_from_path(cTree, path[:depth-1])
            cTree.add_edge(internalNode, precNode, weight=1/(2 ** (depth-1)))
            depth += 1
