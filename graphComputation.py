from igraph import *
from ipdb import set_trace
from itertools import repeat
import pandas as pd

def getNumClasses(url, total_dep_pname):
    # CALCOLARE IL NUMERO DI CLASSI IMPLICATE NEL CICLO
    # CONSIDERARE IL FATTO CHE NEI TINY LE CLASSI SI RIPETONO, QUINDI CONTROLLARE IN E OUT
    class_dependencies = getClassesInPackDep(url, total_dep_pname)
    class_dependencies_p1 = class_dependencies.drop_duplicates(['AS', 'pack_1'])
    class_dependencies_p2 = class_dependencies.drop_duplicates(['AS', 'pack_2'])
    count_p1 = class_dependencies_p1.groupby(['AS']).count()['pack_1'].to_frame()
    count_p2 = class_dependencies_p2.groupby(['AS']).count()['pack_2'].to_frame()
    join_count = count_p1.join(count_p2)
    join_count.index.name = None
    join_count['num_classes'] = join_count['pack_1'] + join_count['pack_2']
    join_count.drop(['pack_1', 'pack_2'], axis=1, inplace=True)
    join_count['AS'] = join_count.index
    return join_count, class_dependencies

def getClassesInPackDep(url, total_dep_pname):
    g = Graph.Read_GraphML(url)
    sysPackage = g.vs.select(PackageType_eq="SystemPackage")
    total_dep_pname.sort_values('AS', inplace=True)
    total_dep_pname['duplicate'] = total_dep_pname.duplicated('AS', False)
    class_dependencies = []
    for index, row in total_dep_pname.iterrows():
        # COLONNE INVERTITE PERCHE' L'ORDINE NELLE CIRCLE E' INVERTITO - CONTROLLARE FILE "packageOrderInCDSmell"
        p1 = g.vs.select(name_eq=row['pack_1'])[0]
        p2 = g.vs.select(name_eq=row['pack_2'])[0]
        classesInPack_p1 = getVertecesNameByEdgeLabel(
            g, p1, 'belongsTo', 'IN')
        classesInPack_p2 = getVertecesNameByEdgeLabel(
            g, p2, 'belongsTo', 'IN')
        # CASO CIRCLE - DIPENDENZE IN UNA DIREZIONE SOLA
        if row['duplicate']:
            total_depClasses = []
            for j in classesInPack_p1:
                classDep = getVertecesNameByEdgeLabel(g, j, 'dependsOn', 'OUT')
                classImpl = getVertecesNameByEdgeLabel(g, j, 'isImplementationOf', 'OUT')
                if len(classDep)==0 and len(classImpl)==0:
                    continue
                elif len(classDep) > 0 and len(classImpl) == 0:
                    total_depClasses.extend(list(zip(repeat(j), classDep, repeat('OUT'))))
                elif len(classDep) == 0 and len(classImpl) > 0:
                    total_depClasses.extend(list(zip(repeat(j), classImpl, repeat('OUT'))))
                else:
                    total_depClasses.extend(list(zip(repeat(j), classDep, repeat('OUT'))))
                    total_depClasses.extend(list(zip(repeat(j), classImpl, repeat('OUT'))))
            classDepBetweenPack = []
            for h in total_depClasses:
                if h[1] in classesInPack_p2:
                    classDepBetweenPack.append(h)
            listOfTuples = list(zip(repeat(row['AS']), classDepBetweenPack))
            class_dependencies.extend(listOfTuples)
        # CASO TINY - DIPENDENZE IN ENTRAMBE LE DIREZIONI
        else:
            total_depClasses_in = []
            total_depClasses_out = []
            for v in classesInPack_p1:
                classDep = getVertecesNameByEdgeLabel(g, v, 'dependsOn', 'OUT')
                classImpl = getVertecesNameByEdgeLabel(g, v, 'isImplementationOf', 'OUT')
                if len(classDep) == 0 and len(classImpl) == 0:
                    continue
                elif len(classDep) > 0 and len(classImpl) == 0:
                    total_depClasses_in.extend(list(zip(repeat(v), classDep, repeat('OUT'))))
                elif len(classDep) == 0 and len(classImpl) > 0:
                    total_depClasses_in.extend(list(zip(repeat(v), classImpl, repeat('OUT'))))
                else:
                    total_depClasses_in.extend(list(zip(repeat(v), classDep, repeat('OUT'))))
                    total_depClasses_in.extend(list(zip(repeat(v), classImpl, repeat('OUT'))))
            for w in classesInPack_p1:
                classDep = getVertecesNameByEdgeLabel(g, w, 'dependsOn', 'IN')
                classImpl = getVertecesNameByEdgeLabel(g, w, 'isImplementationOf', 'IN')
                if len(classDep) == 0 and len(classImpl) == 0:
                    continue
                elif len(classDep) > 0 and len(classImpl) == 0:
                    total_depClasses_out.extend(list(zip(repeat(w), classDep, repeat('IN'))))
                elif len(classDep) == 0 and len(classImpl) > 0:
                    total_depClasses_out.extend(list(zip(repeat(w), classImpl, repeat('IN'))))
                else:
                    total_depClasses_out.extend(list(zip(repeat(w), classDep, repeat('IN'))))
                    total_depClasses_out.extend(list(zip(repeat(w), classImpl, repeat('IN'))))
            classDepBetweenPack_in = []
            classDepBetweenPack_out = []
            for p in total_depClasses_in:
                if p[1] in classesInPack_p2:
                    classDepBetweenPack_in.append(p)
            for k in total_depClasses_out:
                if k[1] in classesInPack_p2:
                    classDepBetweenPack_out.append(k)
            listOfTuples_in = list(zip(repeat(row['AS']), classDepBetweenPack_in))
            listOfTuples_out = list(zip(repeat(row['AS']), classDepBetweenPack_out))
            class_dependencies.extend(listOfTuples_in)
            class_dependencies.extend(listOfTuples_out)
    
    class_dependencies_flatten = [(a, b, c, d) for a, (b, c, d) in class_dependencies]
    class_dependencies_df = pd.DataFrame(class_dependencies_flatten, columns=['AS', 'pack_1', 'pack_2', 'direction'])
    return class_dependencies_df

def getVertecesByEdgeLabel(g, p, label, direction):
    belongsTo_edges = getEdgesByLabel(g, p, label, direction)
    if len(belongsTo_edges) == 1:
        if direction == 'IN':
            return g.vs[belongsTo_edges[0].source]
        elif direction == 'OUT':
            return g.vs[belongsTo_edges[0].target]

    vertexDep = []
    if direction == 'IN':
        for i in range(len(belongsTo_edges)):
            vertexDep.append(g.vs[belongsTo_edges[i].source])
    elif direction == 'OUT':
        for i in range(len(belongsTo_edges)):
            vertexDep.append(g.vs[belongsTo_edges[i].target])
    return vertexDep


def getVertecesNameByEdgeLabel(g, p, label, direction):
    belongsTo_edges = getEdgesByLabel(g, p, label, direction)
    
    vertexDep = []
    if len(belongsTo_edges)==0:
        return vertexDep
    if direction == 'IN':
        for i in range(len(belongsTo_edges)):
            vertexDep.append(g.vs[belongsTo_edges[i].source]['name'])
    elif direction == 'OUT':
        for i in range(len(belongsTo_edges)):
            vertexDep.append(g.vs[belongsTo_edges[i].target]['name'])
    return vertexDep

def getEdgesByLabel(g, vertex, label, direction):
    belongsTo_edges = g.es[g.incident(vertex, mode=direction)].select(labelE_eq=label)
    return belongsTo_edges
