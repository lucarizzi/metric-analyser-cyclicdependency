import pandas as pd
from ipdb import set_trace
from sklearn import preprocessing


def getSP(adi_df):
    adi_cleaned_df = adi_df[['AS', 'PageRank', 'NumberOfVerteces', 'NumOfCycle']]
    # SEVERITY WITH NUMOFVERTECES * NUMOFCYCLE
    adi_cleaned_df['Severity_elCyc'] = adi_cleaned_df['NumberOfVerteces'] * adi_cleaned_df['NumOfCycle']
    # SEVERITY WITH NUMOFVERTECES
    adi_cleaned_df['Severity_el'] = adi_cleaned_df['NumberOfVerteces']
    # SEVERITY WITH NUMOFCYCLE
    adi_cleaned_df['Occurrence'] = adi_cleaned_df['NumOfCycle']
    adi_cleaned_df.drop(['NumberOfVerteces', 'NumOfCycle'], axis=1, inplace=True)

    # SCALE VALUES INTO RANGE [0-1] - END
    adi_cleaned_df.sort_values(by=['AS'], inplace=True)

    return adi_cleaned_df
