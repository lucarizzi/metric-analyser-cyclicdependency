import logging, ctGenerator, itertools, operator, statistics
from igraph import *
from ipdb import set_trace


def getDiameter(url, ct_out_dir):
    g = Graph.Read_GraphML(url)
    containmentTree = ctGenerator.getCT(g, url, ct_out_dir)

    # START DESIRABILITY RANK
    rank_max, total_dep_pname = generateRank(g, containmentTree)

    # START STATISTICS
    return rank_max, total_dep_pname

def groupBy_max(l):
    it = itertools.groupby(l, operator.itemgetter(0))
    for key, subiter in it:
       yield key, max(item[3] for item in subiter)

def generateRank(pTree, cTree):
    nodes_over_threshold_tiny, nodes_over_threshold_tiny_pname, tiny_rank = generateRankTiny(pTree, cTree)
    nodes_over_threshold_circle, nodes_over_threshold_circle_pname, circle_rank = generateRankCircle(pTree, cTree)
    
    total_dep_pname = nodes_over_threshold_tiny_pname.copy()
    total_dep_pname.extend(nodes_over_threshold_circle_pname)
    gb_max = list(groupBy_max(nodes_over_threshold_circle_pname))

    rank_max = tiny_rank.copy()
    rank_max.extend(gb_max)

    return rank_max, total_dep_pname


def generateRankTiny(pTree, cTree):
    tiny_smell = pTree.vs.select(labelV_eq='smell', smellType_eq='cyclicDep', vertexType='package', numCycleVertices_eq=2)
    diameter_list = []
    nodes_over_threshold = []
    nodes_over_threshold_pname = []

    for i in tiny_smell:
        target_nodes = []
        # estraggo per ogni singolo nodo smell tiny gli archi con label partOfCycle
        incident_edges = pTree.incident(i, mode=OUT)
        poc_edge = extract_edge(pTree, incident_edges, 'partOfCycle')
        # trovo i nodi target degli archi incidenti
        for t in poc_edge:
            target_nodes.append(t.target)
        # trovo i nodi target nell'albero tramite l'id trovato
        t1 = pTree.vs[target_nodes[0]]
        t2 = pTree.vs[target_nodes[1]]

        # controllo che non ci siano package di tipo retrieved
        if t1["PackageType"] == "RetrievedPackage" or t2["PackageType"] == "RetrievedPackage":
            logging.error('RETRIEVED PACKAGE FOUNDED')
            continue


        package1 = cTree.vs.select(id_eq=t1['id'])[0]
        package2 = cTree.vs.select(id_eq=t2['id'])[0]

        e_path = cTree.get_shortest_paths(
            package1.index, to=package2.index, mode=ALL, output='epath')

        # controllo che non vengano trovati differenti shorthest_path
        if len(e_path) > 1:
            logging.error('MULTIPLE SHORTHEST_PATHS FOUNDED')
            continue
        # calcolo la metrica di diameter
        diameter = get_weight(e_path[0], cTree)
        id_smell = i['id']
        tuple = (id_smell, diameter)
        if tuple[1] >= 0:
            tuple_complete = (id_smell, package1['id'], package2['id'], diameter)
            pTree_package1 = pTree.vs.select(id_eq=package1['id'])[0]
            pTree_package2 = pTree.vs.select(id_eq=package2['id'])[0]
            tuple_complete_pname = (id_smell, pTree_package1['name'], pTree_package2['name'], diameter)
            nodes_over_threshold.append(tuple_complete)
            nodes_over_threshold_pname.append(tuple_complete_pname)
        diameter_list.append(tuple)

    return nodes_over_threshold, nodes_over_threshold_pname, diameter_list


def generateRankCircle(pTree, cTree):
    circle_smell = pTree.vs.select(labelV_eq='smell', smellType_eq='cyclicDep', vertexType='package', numCycleVertices_gt=2)
    diameter_list = []
    nodes_over_threshold = []
    nodes_over_threshold_pname = []
    for i in circle_smell:
        id_smell = i['id']
        # estraggo per ogni singolo nodo smell circle gli archi con label partOfCycle
        incident_edges = pTree.incident(i, mode=OUT)
        poc_edge = extract_edge(pTree, incident_edges, 'partOfCycle')

        # ordino gli archi uscenti dal nodo smell per l'attributo "orderInCycle"
        sorted_poc_edge = [None] * len(poc_edge)
        for j in poc_edge:
            index = int(j.attributes()["orderInCycle"])
            sorted_poc_edge[index] = j
        sorted_poc_edge[1:] = reversed(sorted_poc_edge[1:])

        # calcolo il diameter per ogni coppia di NODI
        weight_list_tiny = []
        for x in range(len(sorted_poc_edge)-1):
            # estraggo gli indici dei nodi target
            target_node1 = sorted_poc_edge[x].target
            target_node2 = sorted_poc_edge[x+1].target

            # trovo i nodi target nell'albero tramite l'indice trovato
            t1 = pTree.vs[target_node1]
            t2 = pTree.vs[target_node2]

            # controllo che non ci siano package di tipo retrieved
            if t1["PackageType"] == "RetrievedPackage" or t2["PackageType"] == "RetrievedPackage":
                logging.error('RETRIEVED PACKAGE FOUNDED')
                continue

            # estraggo i nodi corrispondenti a quelli trovati nel containment tree
            package1 = cTree.vs.select(id_eq=t1['id'])[0]
            package2 = cTree.vs.select(id_eq=t2['id'])[0]

            e_path = cTree.get_shortest_paths(
                package1.index, to=package2.index, mode=ALL, output='epath')

            # controllo che non vengano trovati differenti shorthest_path
            if len(e_path) > 1:
                logging.error('MULTIPLE SHORTHEST_PATHS FOUNDED')
                continue

            weight = get_weight(e_path[0], cTree)
            if weight >= 0:
                tuple = (id_smell, package1['id'], package2['id'], weight)
                pTree_package1 = pTree.vs.select(id_eq=package1['id'])[0]
                pTree_package2 = pTree.vs.select(id_eq=package2['id'])[0]
                tuple_complete_pname = (
                    id_smell, pTree_package1['name'], pTree_package2['name'], weight)
                nodes_over_threshold.append(tuple)
                nodes_over_threshold_pname.append(tuple_complete_pname)
            weight_list_tiny.append(weight)

        # calcolo il diameter tra l'ultimo nodo e il primo del ciclo
        start_node = sorted_poc_edge[0].target
        final_node = sorted_poc_edge[len(sorted_poc_edge)-1].target

        sn = pTree.vs[start_node]
        fn = pTree.vs[final_node]

        start_package = cTree.vs.select(id_eq=sn['id'])[0]
        final_package = cTree.vs.select(id_eq=fn['id'])[0]

        e_path_fs = cTree.get_shortest_paths(
            start_package.index, to=final_package.index, mode=ALL, output='epath')

        # controllo che non vengano trovati differenti shorthest_path
        if len(e_path) > 1:
            logging.error('MULTIPLE SHORTHEST_PATHS FOUNDED')
            continue

        weight_fs = get_weight(e_path_fs[0], cTree)
        if weight_fs >= 0:
            tuple_id = (
                id_smell, final_package['id'], start_package['id'], weight_fs)
            pTree_package1 = pTree.vs.select(id_eq=final_package['id'])[0]
            pTree_package2 = pTree.vs.select(id_eq=start_package['id'])[0]
            tuple_pname = (
                id_smell, pTree_package1['name'], pTree_package2['name'], weight_fs)
            nodes_over_threshold.append(tuple_id)
            nodes_over_threshold_pname.append(tuple_pname)
        weight_list_tiny.append(weight_fs)

        diameter = max(weight_list_tiny)

        tuple = (id_smell, diameter)
        diameter_list.append(tuple)
    return nodes_over_threshold, nodes_over_threshold_pname, diameter_list

# extraction of edges with a specific label
def extract_edge(pTree, edges, label):
    labelled_edge = []
    for i in edges:
        if pTree.es[i]['labelE'] == label:
            labelled_edge.append(pTree.es[i])
    return labelled_edge

# diameter metric computation
def get_weight(e_path, cTree):
    list_edge = []
    for i in e_path:
        list_edge.append(cTree.es[i])

    weight = 0
    for j in list_edge:
        w = j['weight']
        weight += w
    return weight
